package vn.vnpt_technology.ssdc.erp;

import org.apache.log4j.Logger;
import vn.vnpt_technology.ssdc.erp.migrates.Transfer;

public class App {

    final static Logger logger = Logger.getLogger(App.class);

    public static void main(String[] args) {
        logger.info("--START MIGRATE--");
        Transfer migrateTransfer = new Transfer();
        migrateTransfer.migrateShipment();
        logger.info("--END MIGRATE--");
    }
}
