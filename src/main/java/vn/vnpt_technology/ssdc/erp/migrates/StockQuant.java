package vn.vnpt_technology.ssdc.erp.migrates;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import vn.vnpt_technology.ssdc.erp.utils.FileUtils;
import vn.vnpt_technology.ssdc.erp.utils.RequestUtils;

import java.util.HashMap;
import java.util.Map;

public class StockQuant {

    final static Logger logger = Logger.getLogger(StockQuant.class);

    private String API_KEY = "Bearer ";

    private static final String HOST_ERP1 = FileUtils.readConfig("HOST_ERP1");
    private static final String HOST_ERP2 = FileUtils.readConfig("HOST_ERP2");

    private String SEARCH_PRODUCT = FileUtils.readConfig("SEARCH_PRODUCT");
    private String SEARCH_COMPANY = FileUtils.readConfig("SEARCH_COMPANY");
    private String SEARCH_PRODUCT_VERSION = FileUtils.readConfig("SEARCH_PRODUCT_VERSION_FOR_SHIPMENT");

    private String GET_STOCK_QUANT_LOCATION = FileUtils.readConfig("GET_STOCK_QUANT_LOCATION");
    private String API_STOCK_QUANT = FileUtils.readConfig("API_STOCK_QUANT");
    private String SEARCH_LOCATION_BY_BARCODE = FileUtils.readConfig("SEARCH_LOCATION_BY_BARCODE");

    public JSONArray listOldLocation = null;
    Map<String, JSONObject> mapMan = new HashMap<>();
    Map<String, JSONObject> mapProduct = new HashMap<>();
    Map<String, JSONObject> mapProductVersion = new HashMap<>();
    Map<String, JSONObject> mapLocation = new HashMap<>();

    public StockQuant() {
        init();
    }

    private void init(){
        //On top of that, import Location
        Location location = new Location();
        location.migrate();

        this.listOldLocation = location.listOldLocation;

        //init
        API_KEY = API_KEY + RequestUtils.requestToken();

        SEARCH_PRODUCT = HOST_ERP2 + SEARCH_PRODUCT;
        SEARCH_COMPANY = HOST_ERP2 + SEARCH_COMPANY;
        SEARCH_PRODUCT_VERSION = HOST_ERP2 + SEARCH_PRODUCT_VERSION;
        GET_STOCK_QUANT_LOCATION = HOST_ERP1 + GET_STOCK_QUANT_LOCATION;
        API_STOCK_QUANT = HOST_ERP2 + API_STOCK_QUANT;
        SEARCH_LOCATION_BY_BARCODE = HOST_ERP2 + SEARCH_LOCATION_BY_BARCODE;
    }

    private JSONObject buildStockQuant(JSONObject oldData){
        JSONObject stockQuant = new JSONObject();
        try{
            //search man, search product, search product version
            JSONObject man = null;
            JSONObject product = null;
            JSONObject productVersion = null;
            JSONObject location = null;

            if(!oldData.isNull("manufacturer_id") && mapMan.get(oldData.get("manufacturer_id")) == null){
                man = RequestUtils.requestGET(SEARCH_COMPANY + oldData.get("manufacturer_id"), API_KEY, null).getJSONObject(0);
                mapMan.put(oldData.get("manufacturer_id").toString(), man);
            }else if(!oldData.isNull("manufacturer_id") && mapMan.get(oldData.get("manufacturer_id")) != null){
                man = mapMan.get(oldData.get("manufacturer_id"));
            }
            if(!oldData.isNull("part_id") &&  mapProduct.get(oldData.get("part_id")) == null){
                product = RequestUtils.requestGET(SEARCH_PRODUCT + oldData.get("part_id"), API_KEY, null).getJSONObject(0);
                mapProduct.put(oldData.get("part_id").toString(), product);
            }else if(!oldData.isNull("part_id") &&  mapProduct.get(oldData.get("part_id")) != null){
                product = mapProduct.get(oldData.get("part_id"));
            }
            if(!oldData.isNull("product_id") && mapProductVersion.get(oldData.get("product_id")) == null){
                productVersion = RequestUtils.requestGET(SEARCH_PRODUCT_VERSION + oldData.get("product_id"), API_KEY, null).getJSONObject(0);
                mapProductVersion.put(oldData.get("product_id").toString(), productVersion);
            }else if(!oldData.isNull("product_id") && mapProductVersion.get(oldData.get("product_id")) != null){
                productVersion = mapProductVersion.get(oldData.get("product_id"));
            }
            if(mapLocation.get(oldData.get("location_id")) == null){
                location = RequestUtils.requestGET(SEARCH_LOCATION_BY_BARCODE+ oldData.get("location_id") + "-*\"", API_KEY, null).getJSONObject(0);
                mapLocation.put(oldData.get("location_id").toString(), location);
            }else{
                location = mapLocation.get(oldData.get("location_id"));
            }

            stockQuant.put("locationId", location.get("id"));
            if(product != null) stockQuant.put("productId", product.get("id"));
            if(productVersion != null) stockQuant.put("productVersionId", productVersion.get("id"));
            if(man != null) stockQuant.put("manId", man.get("id"));
            stockQuant.put("onHand", oldData.get("part_quantity"));
            if("2".equals(oldData.get("inv_package_unit_id").toString())){
                stockQuant.put("traceNumber", oldData.get("code"));
                stockQuant.put("packageNumber", oldData.get("parent_code"));
            }else{
                stockQuant.put("packageNumber", oldData.get("code"));
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("Error when migrate stock quantity at oldData: " +oldData.toString());
        }

        return  stockQuant;
    }

    private void migrateStockQuantsLocation(String locationId){
        JSONArray result = RequestUtils.requestGET(GET_STOCK_QUANT_LOCATION + locationId, null, null);
        for(int i = 0; i < result.length(); i++){
            JSONObject stockQuant = buildStockQuant(result.getJSONObject(i));
            RequestUtils.requestPOST(API_STOCK_QUANT, API_KEY,stockQuant.toString());
        }
    }

    public void migrate(){
        JSONArray oldLocation = new Location().getOldData();
        for(int i = 0; i < 100; i++){
            try{
                logger.info("START at locationId: " + oldLocation.getJSONObject(i).get("id"));
                migrateStockQuantsLocation(oldLocation.getJSONObject(i).get("id").toString());
                logger.info("DONE at locationId: " + oldLocation.getJSONObject(i).get("id"));
            }catch (Exception e){
                e.printStackTrace();
                logger.error("Error when migrate stock quantity at oldLocationId: " +
                        oldLocation.getJSONObject(i).get("id").toString());
            }
        }
    }

    public static void main(String[] args) {
        StockQuant stockQuant = new StockQuant();
        stockQuant.migrate();
    }
}
