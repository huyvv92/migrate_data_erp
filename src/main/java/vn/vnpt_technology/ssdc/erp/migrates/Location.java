package vn.vnpt_technology.ssdc.erp.migrates;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import vn.vnpt_technology.ssdc.erp.utils.FileUtils;
import vn.vnpt_technology.ssdc.erp.utils.RequestUtils;

public class Location {

    final static Logger logger = Logger.getLogger(Location.class);

    public String API_KEY = "Bearer ";

    public static final String HOST_ERP1 = FileUtils.readConfig("HOST_ERP1");
    public static final String HOST_ERP2 = FileUtils.readConfig("HOST_ERP2");

    public String GET_LOCATIONS = FileUtils.readConfig("GET_LOCATIONS");

    public String API_LOCATION = FileUtils.readConfig("API_LOCATION");

    public String SEARCH_LOCATION_HQV = FileUtils.readConfig("SEARCH_LOCATION_HQV");
    public String SEARCH_LOCATION_HL = FileUtils.readConfig("SEARCH_LOCATION_HL");
    public String SEARCH_OLD_PARENT = FileUtils.readConfig("SEARCH_OLD_PARENT");
    JSONObject locationHQV = null;
    JSONObject locationHL = null;

    public JSONArray listOldLocation = null;

    public Location(){
        init();
    }

    public void init(){
        API_KEY = API_KEY + RequestUtils.requestToken();
        GET_LOCATIONS = HOST_ERP1 +  GET_LOCATIONS;
        API_LOCATION = HOST_ERP2 + API_LOCATION;

        SEARCH_LOCATION_HQV = HOST_ERP2 + SEARCH_LOCATION_HQV;
        SEARCH_LOCATION_HL = HOST_ERP2 + SEARCH_LOCATION_HL;
        SEARCH_OLD_PARENT = HOST_ERP2 + SEARCH_OLD_PARENT;
        locationHQV = RequestUtils.requestGET(SEARCH_LOCATION_HQV, API_KEY, null).getJSONObject(0);
        locationHL = RequestUtils.requestGET(SEARCH_LOCATION_HL, API_KEY, null).getJSONObject(0);
    }

    public JSONObject buildLocation(JSONObject input){
        JSONObject result = new JSONObject();
        result.put("active", true);
        result.put("createdBy","admin");
        result.put("updatedBy","admin");
        result.put("name", input.get("name"));

        if("0".equals(input.get("parent_id").toString())){
            if("1".equals(input.get("inventory_id").toString())){
                result.put("parentId", locationHQV.get("id"));
            }else{
                result.put("parentId", locationHL.get("id"));
            }
        }else{
            JSONObject oldParent = RequestUtils.requestGET(SEARCH_OLD_PARENT + input.get("parent_id")+"-*", API_KEY, null).getJSONObject(0);
            result.put("parentId", oldParent.get("id"));
        }

        result.put("returnedLocation", false);
        result.put("scrapLocation", false);
        result.put("type","internal");
        result.put("barcode", input.get("id")+"-"+input.get("name"));
        return result;
    }

    public JSONArray getOldData(){
        JSONArray lists = null;
        try{
            lists = RequestUtils.requestGET(GET_LOCATIONS, null, null);
            this.listOldLocation = lists;
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return lists;
    }

    public void migrate(){
        JSONArray lists = getOldData();
        for(int i = 0; i < lists.length(); i++){
            JSONObject location = lists.getJSONObject(i);
            logger.info("START: " + location.get("name"));
            JSONObject nLocation = buildLocation(location);
            RequestUtils.requestPOST(API_LOCATION, API_KEY, nLocation.toString());
            logger.info("DONE: " + location.get("name"));
        }
    }

    public static void main(String[] args) {
        logger.info("--START MIGRATE--");
        Location migrate = new Location();
        migrate.migrate();
        logger.info("--END MIGRATE--");
    }
}
