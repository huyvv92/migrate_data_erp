package vn.vnpt_technology.ssdc.erp.migrates;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import vn.vnpt_technology.ssdc.erp.utils.FileUtils;
import vn.vnpt_technology.ssdc.erp.utils.RequestUtils;

public class ProductionOrders {

    final static Logger logger = Logger.getLogger(Location.class);

    private String API_KEY = "Bearer ";

    private static final String HOST_ERP1 = FileUtils.readConfig("HOST_ERP1");
    private static final String HOST_ERP2 = FileUtils.readConfig("HOST_ERP2");

    private String SEARCH_OP_HQV_FACTORY = FileUtils.readConfig("SEARCH_OP_HQV_FACTORY");
    private String SEARCH_OP_HL_FACTORY = FileUtils.readConfig("SEARCH_OP_HL_FACTORY");
    private JSONObject opHQV = new JSONObject();
    private JSONObject opHL = new JSONObject();

    private String GET_INFO_PRODUCTION_ORDERS = FileUtils.readConfig("GET_INFO_PRODUCTION_ORDERS");
    private String GET_LIST_PART_PRODUCTION_ORDER = FileUtils.readConfig("GET_LIST_PART_PRODUCTION_ORDER");

    private String SEARCH_PRODUCT_VERSION = FileUtils.readConfig("SEARCH_PRODUCT_VERSION");
    private String SEARCH_PRODUCT = FileUtils.readConfig("SEARCH_PRODUCT");
    private String SEARCH_COMPANY = FileUtils.readConfig("SEARCH_COMPANY");

    private String API_TRANSFER = FileUtils.readConfig("API_TRANSFER");

    public ProductionOrders(){
        init();
    }

    private void init(){
        API_KEY = API_KEY + RequestUtils.requestToken();

        SEARCH_OP_HQV_FACTORY = HOST_ERP2 + SEARCH_OP_HQV_FACTORY;
        SEARCH_OP_HL_FACTORY = HOST_ERP2 + SEARCH_OP_HL_FACTORY;

        opHQV = RequestUtils.requestGET(SEARCH_OP_HQV_FACTORY, API_KEY, null).getJSONObject(0);
        opHL = RequestUtils.requestGET(SEARCH_OP_HL_FACTORY, API_KEY, null).getJSONObject(0);

        GET_INFO_PRODUCTION_ORDERS = HOST_ERP1 + GET_INFO_PRODUCTION_ORDERS;
        GET_LIST_PART_PRODUCTION_ORDER = HOST_ERP1 + GET_LIST_PART_PRODUCTION_ORDER;

        SEARCH_PRODUCT_VERSION = HOST_ERP2 + SEARCH_PRODUCT_VERSION;
        SEARCH_PRODUCT = HOST_ERP2 + SEARCH_PRODUCT;
        SEARCH_COMPANY = HOST_ERP2 + SEARCH_COMPANY;

        API_TRANSFER = HOST_ERP2 + API_TRANSFER;
    }

    public JSONObject buildTransferItemNoneLevel(JSONObject part, JSONObject op){
        JSONObject item = new JSONObject();
        try{
            item.put("active", true);
            item.put("destLocationId", op.get("defaultDestLocationId"));
            item.put("srcLocationId", op.get("defaultSrcLocationId"));
            item.put("initialQuantity", part.get("quantity"));

            JSONObject product = RequestUtils.requestGET(SEARCH_PRODUCT + part.get("part_id"), API_KEY, null).getJSONObject(0);

            item.put("productId", product.get("id"));
            item.put("productName", product.get("name"));
            item.put("reserved", part.get("quantity"));
        }catch (Exception e){
            System.out.println("part error" + part.get("part_id"));
            e.printStackTrace();
        }

        return item;
    }

    public JSONArray buildTransferItemHasLevel(JSONObject level, JSONObject op, JSONObject pOrders){
        JSONArray result = new JSONArray();
        try{
            JSONObject jLevel = new JSONObject();
            jLevel.put("level", level.get("level"));
            float quantity = Float.parseFloat(level.get("qty_per_product").toString()) *
                    Float.parseFloat(pOrders.get("product_quantity").toString());
            jLevel.put("reserved", quantity);
            jLevel.put("initialQuantity", quantity);
            jLevel.put("levelQuantity", level.get("qty_per_product"));
            jLevel.put("selected", true);
            result.put(jLevel);

            JSONArray parts = level.getJSONArray("part");
            for(int i = 0; i < parts.length(); i++){
                //put part
                JSONObject currPart = parts.getJSONObject(i);
                System.out.println("currPart.get(\"part_id\")" + currPart.get("part_id"));
                JSONObject product = RequestUtils.requestGET(SEARCH_PRODUCT + currPart.get("part_id"), API_KEY, null).getJSONObject(0);
                //JSONObject tmp_part = new JSONObject();
                //tmp_part.put("active", true);
                //tmp_part.put("destLocationId", op.get("defaultDestLocationId"));
                //tmp_part.put("srcLocationId", op.get("defaultSrcLocationId"));
                //tmp_part.put("productId", product.get("id"));
                //tmp_part.put("productName", product.get("name"));
                //tmp_part.put("level", level.get("level"));
                //result.put(tmp_part);

                //put man
                JSONArray mans = currPart.getJSONArray("man");
                for(int j = 0; j < mans.length(); j++){
                    JSONObject currMan = mans.getJSONObject(j);
                    JSONObject tmp_man = new JSONObject();
                    tmp_man.put("active", true);
                    tmp_man.put("destLocationId", op.get("defaultDestLocationId"));
                    tmp_man.put("srcLocationId", op.get("defaultSrcLocationId"));
                    tmp_man.put("productId", product.get("id"));
                    tmp_man.put("productName", product.get("name"));
                    tmp_man.put("level", level.get("level"));
                    System.out.println("currMan.get(\"man_id\")" + currMan.get("man_id"));
                    JSONObject man = RequestUtils.requestGET(SEARCH_COMPANY + currMan.get("man_id"), API_KEY, null).getJSONObject(0);
                    tmp_man.put("manId", man.get("id"));
                    tmp_man.put("levelQuantity", level.get("qty_per_product"));
                    tmp_man.put("selected", true);
                    tmp_man.put("internalReference", product.get("name").toString()+man.get("companyCode").toString());
                    tmp_man.put("productDescription", product.get("description"));
                    result.put(tmp_man);

                    /*//put man Pn
                    JSONArray manPns = currMan.getJSONArray("man_pn");
                    for(int k = 0; k < manPns.length(); k++){
                        JSONObject tmp_pn = new JSONObject();
                        tmp_pn.put("active", true);
                        tmp_pn.put("destLocationId", op.get("defaultDestLocationId"));
                        tmp_pn.put("srcLocationId", op.get("defaultSrcLocationId"));
                        tmp_pn.put("productId", product.get("id"));
                        tmp_pn.put("productName", product.get("name"));
                        tmp_pn.put("level", level.get("level"));
                        tmp_pn.put("manId", man.get("id"));
                        if(manPns.get(k) != null && !manPns.get(k).equals("")){
                            tmp_pn.put("manPn", manPns.get(k));
                        }
                        result.put(tmp_pn);
                    }*/
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    private JSONObject buildTransfer(String productionOrdersId){
        JSONObject transfer = new JSONObject();
        try{
            JSONObject productionOrders = RequestUtils.requestGET(GET_INFO_PRODUCTION_ORDERS + productionOrdersId, null);
            JSONObject listParts = RequestUtils.requestGET(GET_LIST_PART_PRODUCTION_ORDER + productionOrdersId, null);

            JSONObject op = null;
            if("3".equals(productionOrders.get("factory_id"))){
                op = opHQV;
            }else{
                op = opHL;
            }

            //build TransferItems
            JSONArray level = listParts.getJSONArray("level");
            JSONArray noneLevel = listParts.getJSONArray("none_level");
            JSONArray transferItems = new JSONArray();
            for(int i = 0; i < noneLevel.length(); i++){
                JSONObject item = noneLevel.getJSONObject(i);
                transferItems.put(buildTransferItemNoneLevel(item, op));
            }
            for(int i = 0; i < level.length(); i++){
                JSONObject item = level.getJSONObject(i);
                JSONArray array = buildTransferItemHasLevel(item, op, productionOrders);
                for(int j = 0; j < array.length(); j++){
                    transferItems.put(array.getJSONObject(j));
                }
            }

            System.out.println("transferItems = " + transferItems);

            //build TransferDetails

            transfer.put("active", true);
            transfer.put("productionQuantity", productionOrders.get("product_quantity"));
            transfer.put("operationTypeId", op.getInt("id"));
            transfer.put("srcLocationId", op.getInt("defaultSrcLocationId"));
            transfer.put("destLocationId", op.getInt("defaultDestLocationId"));
            transfer.put("sourceDocument", productionOrders.get("code"));
            JSONObject productVersion = RequestUtils.requestGET(SEARCH_PRODUCT_VERSION + productionOrders.get("product_id"), API_KEY, null).getJSONObject(0);
            transfer.put("productVersionId", productVersion.get("id"));
           // transfer.put("otherProjects", "");

            transfer.put("transferItems", transferItems);
        }catch (Exception e){
            e.printStackTrace();
        }

        return transfer;
    }

    public void migrate(){
        JSONObject transfer = buildTransfer("500");
        System.out.println("transfer = " + transfer);
        RequestUtils.requestPOST(API_TRANSFER, API_KEY, transfer.toString());
    }

    public static void main(String[] args) {
        ProductionOrders m = new ProductionOrders();
        m.migrate();
    }
}
