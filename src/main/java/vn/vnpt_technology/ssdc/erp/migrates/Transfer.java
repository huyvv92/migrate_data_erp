package vn.vnpt_technology.ssdc.erp.migrates;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import vn.vnpt_technology.ssdc.erp.utils.FileUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Transfer {

    final static Logger logger = Logger.getLogger(Transfer.class);

    public static final String HOST_ERP1 = FileUtils.readConfig("HOST_ERP1");
    public static final String HOST_ERP2 = FileUtils.readConfig("HOST_ERP2");

    public static final String API_AUTH = FileUtils.readConfig("API_AUTH");

    public String API_KEY_ERP2 = "Bearer ";
    public String GET_SHIPMENTS = FileUtils.readConfig("GET_SHIPMENTS");
    public String GET_SHIPMENT_PARTS = FileUtils.readConfig("GET_SHIPMENT_PARTS");
    public String API_TRANSFER = FileUtils.readConfig("API_TRANSFER");

    public String LOCATION_VENDOR = FileUtils.readConfig("LOCATION_VENDOR");

    public String SEARCH_COMPANY = FileUtils.readConfig("SEARCH_COMPANY");
    public String SEARCH_PRODUCT = FileUtils.readConfig("SEARCH_PRODUCT");
    public String SEARCH_SUPPLIER = FileUtils.readConfig("SEARCH_SUPPLIER");
    public String SEARCH_PRODUCT_VERSION = FileUtils.readConfig("SEARCH_PRODUCT_VERSION");

    public String SEARCH_OP_HQV = FileUtils.readConfig("SEARCH_OP_HQV");
    public String SEARCH_OP_HL = FileUtils.readConfig("SEARCH_OP_HL");

    public String MIGRATE_ALL_SHIPMENT = FileUtils.readConfig("MIGRATE_ALL_SHIPMENT");
    public String REMOVE_LOG_TRANSFER = FileUtils.readConfig("REMOVE_LOG_TRANSFER");

    private JSONObject initInfo = new JSONObject();

    public Transfer(){
        init();
    }

    public void init(){
        try{
            //get auth token
            String auth_login = "{ \"email\": \"admin\", \"password\": \"admin\", \"rememberMe\": true }";
            JSONObject auth = requestPOST(HOST_ERP2 + API_AUTH, auth_login);

            API_KEY_ERP2 = API_KEY_ERP2 + auth.get("id_token");

            GET_SHIPMENTS = HOST_ERP1 + GET_SHIPMENTS;
            if("true".equals(MIGRATE_ALL_SHIPMENT)){
                GET_SHIPMENTS = GET_SHIPMENTS + "?MIGRATE_ALL_SHIPMENT=true";
            }else{
                GET_SHIPMENTS = GET_SHIPMENTS + "?MIGRATE_ALL_SHIPMENT=false";
            }
            GET_SHIPMENT_PARTS = HOST_ERP1 + GET_SHIPMENT_PARTS;
            REMOVE_LOG_TRANSFER = HOST_ERP1 + REMOVE_LOG_TRANSFER;

            API_TRANSFER = HOST_ERP2 + API_TRANSFER;
            LOCATION_VENDOR = HOST_ERP2 + LOCATION_VENDOR;
            SEARCH_COMPANY = HOST_ERP2 + SEARCH_COMPANY;
            SEARCH_PRODUCT = HOST_ERP2 + SEARCH_PRODUCT;
            SEARCH_SUPPLIER = HOST_ERP2 + SEARCH_SUPPLIER;
            SEARCH_PRODUCT_VERSION = HOST_ERP2 + SEARCH_PRODUCT_VERSION;
            SEARCH_OP_HQV = HOST_ERP2 + SEARCH_OP_HQV;
            SEARCH_OP_HL = HOST_ERP2 + SEARCH_OP_HL;

            JSONObject opHQV = requestGET(SEARCH_OP_HQV, API_KEY_ERP2).getJSONObject(0);
            JSONObject opHL = requestGET(SEARCH_OP_HL, API_KEY_ERP2).getJSONObject(0);
            initInfo.put("opHQV", opHQV);
            initInfo.put("opHL", opHL);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public JSONArray requestGET(String sUrl, String apiKey){
        JSONArray json = null;
        try {
            URL url = new URL(sUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            if(apiKey != null){
                conn.setRequestProperty("Authorization", API_KEY_ERP2);
            }

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            StringBuilder stringBuilder = new StringBuilder();
            while ((output = br.readLine()) != null) {
                stringBuilder.append(output);
            }
            //System.out.println("stringBuilder = " + stringBuilder);

            conn.disconnect();
            json = new JSONArray(stringBuilder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return json;
    }

    public JSONObject requestPOST(String sUrl, String requestBody){
        try{
            URL url = new URL(sUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", API_KEY_ERP2);

            OutputStream os = conn.getOutputStream();
            os.write(requestBody.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            StringBuilder sb = new StringBuilder();
            //System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                //System.out.println(output);
                sb.append(output);
            }

            JSONObject result = new JSONObject(sb.toString());
            conn.disconnect();
            return result;
        }catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }

    public JSONObject getInfoPart(JSONObject part, JSONObject shipment){
        JSONObject info = new JSONObject();

        try{
            //source Location
            JSONObject locationVendor = requestGET(LOCATION_VENDOR, API_KEY_ERP2).getJSONObject(0);
            info.put("location_vendor", locationVendor);

            //Manufacturer
            JSONObject man = requestGET(SEARCH_COMPANY + part.get("manufacturer_id"), API_KEY_ERP2).getJSONObject(0);
            info.put("man", man);

            //ProductId
            JSONObject product = requestGET(SEARCH_PRODUCT + part.get("part_id"), API_KEY_ERP2).getJSONObject(0);
            info.put("product", product);
        }catch (Exception e){
            e.printStackTrace();
        }

        return info;
    }

    public JSONObject buildTransferDetail(JSONObject part, JSONObject info){
        JSONObject detail = new JSONObject();
        try{
            JSONObject op = null;
            if(initInfo.getJSONObject("opHQV") != null){
                op = initInfo.getJSONObject("opHQV");
            }else{
                op = initInfo.getJSONObject("opHL");
            }

            detail.put("active", true);
            detail.put("destLocationId", op.get("defaultDestLocationId"));
            detail.put("reserved", Float.parseFloat(part.get("quantity").toString()) + Float.parseFloat(part.get("foc").toString()));
            detail.put("manId", info.getJSONObject("man").get("id"));
            //check manPn empty or null
            if(part.get("manufacturer_pn") != null && !part.get("manufacturer_pn").equals("")){
                detail.put("manPn", part.get("manufacturer_pn"));
            }
            detail.put("productId", info.getJSONObject("product").get("id"));
            detail.put("productName", info.getJSONObject("product").get("name"));
            detail.put("srcLocationId", op.get("defaultSrcLocationId"));
            detail.put("state", "available");
        }catch (Exception e){
            e.printStackTrace();
        }

        return detail;
    }

    public JSONObject buildTransferItem(JSONObject part, JSONObject info){
        JSONObject item = new JSONObject();
        try{
            JSONObject op = null;
            if(initInfo.getJSONObject("opHQV") != null){
                op = initInfo.getJSONObject("opHQV");
            }else{
                op = initInfo.getJSONObject("opHL");
            }

            item.put("active", true);
            item.put("destLocationId", op.get("defaultDestLocationId"));
            item.put("reserved", Float.parseFloat(part.get("quantity").toString()) + Float.parseFloat(part.get("foc").toString()));
            item.put("initialQuantity", Float.parseFloat(part.get("quantity").toString()) + Float.parseFloat(part.get("foc").toString()));
            item.put("manId", info.getJSONObject("man").get("id"));
            //check manPn empty or null
            if(part.get("manufacturer_pn") != null && !part.get("manufacturer_pn").equals("")){
                item.put("manPn", part.get("manufacturer_pn"));
            }
            item.put("price", part.get("total_price"));
            item.put("productId", info.getJSONObject("product").get("id"));
            item.put("productName", info.getJSONObject("product").get("name"));
            item.put("spq", part.get("spq"));
            item.put("srcLocationId", op.get("defaultSrcLocationId"));
            item.put("state", "available");
        }catch (Exception e){
            e.printStackTrace();
        }

        return item;
    }

    public JSONObject buildTransfer(JSONObject shipment){
        JSONObject transfer = new JSONObject();
        try{
            JSONObject vendor = requestGET(SEARCH_SUPPLIER+shipment.get("supplier_id"), API_KEY_ERP2).getJSONObject(0);
            JSONObject op = null;
            if("1".equals(shipment.get("destination"))){
                op = initInfo.getJSONObject("opHQV");
            }else{
                op = initInfo.getJSONObject("opHL");
            }
            transfer.put("active", true);
            transfer.put("operationTypeId", op.getInt("id"));
            transfer.put("srcLocationId", op.getInt("defaultSrcLocationId"));
            transfer.put("destLocationId", op.getInt("defaultDestLocationId"));
            transfer.put("partnerId", vendor.getInt("id"));
            transfer.put("scheduledDate", convertDate(shipment.get("ready_date").toString()));
            transfer.put("sourceDocument", shipment.get("shipment_code")+ "||" + shipment.get("order_no"));
            //transfer.put("ownerId", "1");

            System.out.println("productverson = " + SEARCH_PRODUCT_VERSION + shipment.get("sys_product_line_id"));
            JSONObject productVersion = requestGET(SEARCH_PRODUCT_VERSION + shipment.get("sys_product_line_id"), API_KEY_ERP2).getJSONObject(0);
            transfer.put("productVersionId", productVersion.get("id"));

            JSONArray parts = requestGET(GET_SHIPMENT_PARTS + shipment.get("id"), null);
            JSONArray items = new JSONArray();
            JSONArray details = new JSONArray();
            for(int i = 0; i < parts.length(); i++){
                JSONObject info = getInfoPart(parts.getJSONObject(i), shipment);
                items.put(buildTransferItem(parts.getJSONObject(i), info));
                details.put(buildTransferDetail(parts.getJSONObject(i), info));
            }
            transfer.put("transferItems", items);
            transfer.put("transferDetails", details);
            //transfer.put("state", "new");
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return transfer;
    }

    public long convertDate(String date){
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date d = f.parse(date);
            long milliseconds = d.getTime();
            return  milliseconds;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void migrateShipment(){
        try{
            JSONArray shipments = requestGET(GET_SHIPMENTS, null);
            for(int i = 0; i < shipments.length(); i++){
                JSONObject shipment = shipments.getJSONObject(i);
                JSONObject transfer = buildTransfer(shipment);
                requestPOST(API_TRANSFER, transfer.toString());

                if(!"true".equals(MIGRATE_ALL_SHIPMENT)){
                    requestGET(REMOVE_LOG_TRANSFER+shipment.get("id"), null);
                }

                logger.info("Done import :" + shipment.get("shipment_code"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception{
        Transfer migrateTransfer = new Transfer();
//        migrateTransfer.init();
        migrateTransfer.migrateShipment();
    }
}
