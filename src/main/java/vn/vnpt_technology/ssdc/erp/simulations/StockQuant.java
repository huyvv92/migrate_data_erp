package vn.vnpt_technology.ssdc.erp.simulations;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import vn.vnpt_technology.ssdc.erp.utils.FileUtils;
import vn.vnpt_technology.ssdc.erp.utils.RequestUtils;

import java.util.HashSet;
import java.util.Set;

public class StockQuant {

    final static Logger logger = Logger.getLogger(StockQuant.class);

    private String API_KEY = "Bearer ";

    private static final String HOST_ERP1 = FileUtils.readConfig("HOST_ERP1");
    private static final String HOST_ERP2 = FileUtils.readConfig("HOST_ERP2");

    private String SEARCH_HQV_STOCK = FileUtils.readConfig("SEARCH_HQV_STOCK");
    private String SEARCH_HL_STOCK = FileUtils.readConfig("SEARCH_HL_STOCK");
    private JSONObject stockHQV = new JSONObject();
    private JSONObject stockHL = new JSONObject();

    private String SEARCH_PRODUCT_MAN_MANPN = FileUtils.readConfig("SEARCH_PRODUCT_MAN_MANPN");

    private String API_STOCK_QUANT = FileUtils.readConfig("API_STOCK_QUANT");
    private String API_PACKAGE = FileUtils.readConfig("API_PACKAGE");
    private String API_LOT = FileUtils.readConfig("API_LOT");

    private String GET_CURRENT_PACKAGE = FileUtils.readConfig("GET_CURRENT_PACKAGE");
    private String GET_CURRENT_LOT = FileUtils.readConfig("GET_CURRENT_LOT");

    private String SEARCH_PRODUCT_FINAL = FileUtils.readConfig("SEARCH_PRODUCT_FINAL");
    private String SEARCH_ITEMS_TRANSFER = FileUtils.readConfig("SEARCH_ITEMS_TRANSFER");
    private String SEARCH_OPTION_PRODUCT = FileUtils.readConfig("SEARCH_OPTION_PRODUCT");

    public StockQuant() {
        init();
    }

    private void init(){
        API_KEY = API_KEY + RequestUtils.requestToken();
        SEARCH_HQV_STOCK = HOST_ERP2 + SEARCH_HL_STOCK;
        SEARCH_HL_STOCK = HOST_ERP2 + SEARCH_HL_STOCK;

        stockHQV = RequestUtils.requestGET(SEARCH_HQV_STOCK, API_KEY, null).getJSONObject(0);
        stockHL = RequestUtils.requestGET(SEARCH_HL_STOCK, API_KEY, null).getJSONObject(0);

        SEARCH_PRODUCT_MAN_MANPN = HOST_ERP2 + SEARCH_PRODUCT_MAN_MANPN;
        API_STOCK_QUANT = HOST_ERP2 + API_STOCK_QUANT;
        API_LOT = HOST_ERP2 + API_LOT;
        API_PACKAGE = HOST_ERP2 + API_PACKAGE;

        GET_CURRENT_PACKAGE= HOST_ERP2 + GET_CURRENT_PACKAGE;
        GET_CURRENT_LOT= HOST_ERP2 + GET_CURRENT_LOT;

        SEARCH_PRODUCT_FINAL= HOST_ERP2 + SEARCH_PRODUCT_FINAL;
        SEARCH_ITEMS_TRANSFER= HOST_ERP2 + SEARCH_ITEMS_TRANSFER;
        SEARCH_OPTION_PRODUCT= HOST_ERP2 + SEARCH_OPTION_PRODUCT;
    }

    private JSONArray getProductManManPN(int transferMOId){
        JSONArray result = new JSONArray();
        //result = RequestUtils.requestGET(SEARCH_PRODUCT_MAN_MANPN+"&size"+size, API_KEY, null);

        JSONArray moItems = RequestUtils.requestGET(SEARCH_ITEMS_TRANSFER+transferMOId+"&size=100000", API_KEY, null);
        Set<Integer> products = new HashSet<>();
        for(int i = 0; i < moItems.length(); i++){
            products.add(moItems.getJSONObject(i).getInt("productId"));
        }

        for(Integer product : products){
            JSONArray pmms = RequestUtils.requestGET(SEARCH_OPTION_PRODUCT+product+"&size=4", API_KEY, null);
            for(int i = 0; i < pmms.length(); i++){
                result.put(pmms.getJSONObject(i));
            }
        }

        return result;
    }

    private JSONArray buildStockQuant(JSONObject pmm, JSONObject stock, JSONObject project){
        JSONArray array = new JSONArray();

        for(int _i = 0; _i < 2; _i++){
            JSONObject pack = RequestUtils.requestGET(GET_CURRENT_PACKAGE, API_KEY);
            JSONObject iPack = new JSONObject();
            iPack.put("packageNumber", pack.get("generatedSequence"));
            iPack = RequestUtils.requestPOST(API_PACKAGE, API_KEY, iPack.toString());
            for(int i = 0; i < 5; i++){
                JSONObject lot = RequestUtils.requestGET(GET_CURRENT_LOT, API_KEY);
                JSONObject iLot = new JSONObject();
                iLot.put("lotNumber", lot.get("generatedSequence"));
                iLot = RequestUtils.requestPOST(API_LOT, API_KEY, iLot.toString());

                JSONObject tmp = new JSONObject();
                tmp.put("locationId", stock.get("id"));
                tmp.put("lotId", iLot.get("id"));
                tmp.put("manId", pmm.get("manufacturerId"));
                if(pmm.get("manufacturerPn") != null && !pmm.get("manufacturerPn").equals("")){
                    tmp.put("manPn", pmm.get("manufacturerPn"));
                }
                tmp.put("onHand", 3000);
                tmp.put("packageId", iPack.get("id"));
                tmp.put("productId", pmm.get("productId"));
                tmp.put("productVersionId", project.get("id"));

                array.put(tmp);
            }
        }

        return array;
    }



    public void simulation(){
        JSONArray pmms = getProductManManPN(228);

        for(int i = 0; i < pmms.length(); i++){
            JSONObject item = pmms.getJSONObject(i);

            JSONObject project1 = RequestUtils.requestGET(SEARCH_PRODUCT_FINAL+4384, API_KEY, null).getJSONObject(0);
            JSONObject project2 = RequestUtils.requestGET(SEARCH_PRODUCT_FINAL+4383, API_KEY, null).getJSONObject(0);

            JSONObject[] proj = {project1, project2};
            JSONObject[] stock = {stockHQV, stockHL};

            for(int j = 0; j < proj.length; j++){
                for(int k = 0; k < stock.length; k++){
                    JSONArray stockQuant = buildStockQuant(item, stock[k], proj[j]);
                    for(int t = 0; t < stockQuant.length(); t++){
                        System.out.println("item = " + stockQuant.getJSONObject(t));
                        RequestUtils.requestPOST(API_STOCK_QUANT, API_KEY, stockQuant.getJSONObject(t).toString());
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        StockQuant sq = new StockQuant();
        sq.simulation();
    }
}
