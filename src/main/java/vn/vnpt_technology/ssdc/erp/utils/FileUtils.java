package vn.vnpt_technology.ssdc.erp.utils;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.*;

public class FileUtils {

    final static Logger logger = Logger.getLogger(FileUtils.class);

    public static String readConfig(String key) {
        File configFile = new File("src/main/resources/config.properties");
        String rtn = key;
        try {
            FileReader reader = new FileReader(configFile);
            Properties props = new Properties();
            props.load(reader);
            rtn = props.getProperty(key);
            reader.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return rtn;
    }



    public static void main(String[] args) {

    }
}
