package vn.vnpt_technology.ssdc.erp.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class RequestUtils {

    private static String coreGETRequest(String sUrl, String apiKey){
        StringBuilder result = new StringBuilder();
        try {
            URL url = new URL(sUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            if(apiKey != null){
                conn.setRequestProperty("Authorization", apiKey);
            }

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            while ((output = br.readLine()) != null) {
                result.append(output);
            }
            //System.out.println("result = " + result);
            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    private static String corePOSTRequest(String sUrl, String apiKey, String requestBody){
        StringBuilder result = new StringBuilder();

        try{
            URL url = new URL(sUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            if(apiKey != null)
                conn.setRequestProperty("Authorization", apiKey);

            OutputStream os = conn.getOutputStream();
            os.write(requestBody.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            while ((output = br.readLine()) != null) {
                result.append(output);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return result.toString();
    }

    public static JSONObject requestGET(String sUrl, String apiKey){
        String response = coreGETRequest(sUrl,apiKey);
        return new JSONObject(response);
    }

    public static JSONArray requestGET(String sUrl, String apiKey, String type){
        String response = coreGETRequest(sUrl,apiKey);
        //System.out.println("response = " + response);
        return new JSONArray(response);
    }

    public static JSONObject requestPOST(String sUrl, String apiKey, String requestBody){
        String response = corePOSTRequest(sUrl,apiKey, requestBody);
        return new JSONObject(response);
    }

    public static JSONArray requestPOST(String sUrl, String apiKey, String requestBody, String type){
        String response = corePOSTRequest(sUrl,apiKey, requestBody);
        return new JSONArray(response);
    }

    public static String requestToken(){
        String auth_login = FileUtils.readConfig("AUTH_LOGIN");
        String HOST = FileUtils.readConfig("HOST_ERP2");
        String API_AUTH = FileUtils.readConfig("API_AUTH");
        JSONObject auth = requestPOST(HOST + API_AUTH, null, auth_login);
        return auth.getString("id_token");
    }

    public static void main(String[] args) {
        String token = RequestUtils.requestToken();
        System.out.println("token = " + token);
    }
}
